using RateLimiter;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddInMemoryRateLimiter(builder.Configuration);

builder.Services.AddControllers();
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new() { Title = "RateLimitedWebApi", Version = "v1" });
});

builder.Services.AddLogging(loggingBuilder =>
{
    loggingBuilder.AddSimpleConsole(formatterOptions =>
    {
        formatterOptions.SingleLine = true;
    });
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "RateLimitedWebApi v1"));
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.UseRateLimiter();

app.MapControllers();

app.Run();

// needed for visibility in test project
public partial class Program { }