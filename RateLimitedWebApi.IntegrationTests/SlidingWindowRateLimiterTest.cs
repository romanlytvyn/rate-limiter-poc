using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace RateLimitedWebApi.SystemTests
{
    public class SlidingWindowRateLimiterTest : IClassFixture<SlidingWindowWebApiAppFactory>
    {
        [Fact]
        public async Task Get_TooFrequently_ReturnsTooManyRequestsResponse()
        {
            var factory = new SlidingWindowWebApiAppFactory();
            var client = factory.CreateClient();

            await client.GetAsync("/weatherForecast/");
            await Task.Delay(1000);

            await client.GetAsync("/weatherForecast/");
            await Task.Delay(1000);

            var response = await client.GetAsync("/weatherForecast/");

            Assert.Equal(HttpStatusCode.TooManyRequests, response.StatusCode);
        }

        [Fact]
        public async Task Get_LessFrequently_ReturnsSuccessResponse()
        {
            var factory = new SlidingWindowWebApiAppFactory();
            var client = factory.CreateClient();

            await client.GetAsync("/weatherForecast/");
            await Task.Delay(8000);

            await client.GetAsync("/weatherForecast/");
            await Task.Delay(8000);
            
            var response = await client.GetAsync("/weatherForecast/");

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }
    }
}