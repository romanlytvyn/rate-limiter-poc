﻿namespace RateLimiter
{
    public class RateLimiterOptions
    {
        public RateLimiterType RateLimiterType { get; set; } = RateLimiterType.Unknown;

        public KeyExtractorType KeyExtractorType { get; set; } = KeyExtractorType.Unknown;

        public int MaxRequestsPerWindow { get; set; } = int.MaxValue;

        public int WindowDurationSeconds { get; set; } = 0;

        public TimeSpan WindowDuration => TimeSpan.FromSeconds(WindowDurationSeconds);

        public bool IsEnabled => RateLimiterType != RateLimiterType.Unknown
                && KeyExtractorType != KeyExtractorType.Unknown
                && MaxRequestsPerWindow < int.MaxValue
                && WindowDurationSeconds > 0;
    }

    public enum RateLimiterType
    {
        Unknown = 0,
        SlidingLog = 1,
        SlidingWindow = 2
    }

    public enum KeyExtractorType
    {
        Unknown = 0,
        Path = 1
    }
}
