﻿using Microsoft.AspNetCore.Http;

namespace RateLimiter
{
    internal class PathRequestKeyExtractor : IRequestKeyExtractor
    {
        public string GetKey(HttpRequest request) => request.Path;
    }
}