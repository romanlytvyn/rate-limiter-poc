﻿using Microsoft.AspNetCore.Http;

namespace RateLimiter
{
    public interface IRequestKeyExtractor
    {
        public string GetKey(HttpRequest request);
    }
}
