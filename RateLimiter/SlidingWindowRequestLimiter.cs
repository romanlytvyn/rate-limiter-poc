﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace RateLimiter
{
    internal class SlidingWindowRequestLimiter : IRateLimiter
    {
        private readonly RateLimiterOptions _options;
        private readonly ISlidingWindowRepository _repository;
        private readonly ILogger<SlidingWindowRequestLimiter> _logger;

        public SlidingWindowRequestLimiter(IOptions<RateLimiterOptions> options,
            ISlidingWindowRepository repository,
            ILogger<SlidingWindowRequestLimiter> logger)
        {
            _options = options.Value;
            _repository = repository;
            _logger = logger;
        }

        public async Task<bool> IsRateLimited(string key)
        {
            var slidingWindow = await _repository.GetSlidingWindow(key);
            var currentTimestamp = DateTime.UtcNow;
            var elapsedSinceWindowStart = currentTimestamp - slidingWindow.StartTimestamp;

            var previousWindowWeight = (_options.WindowDuration - elapsedSinceWindowStart) / _options.WindowDuration;
            var weightedRequestCount = slidingWindow.PreviousCount * previousWindowWeight + slidingWindow.CurrentCount;

            _logger.LogDebug(new
            {
                Timestamp = currentTimestamp,
                WindowStart = slidingWindow.StartTimestamp,
                SinceWindowStart = elapsedSinceWindowStart,
                PreviousCount = slidingWindow.PreviousCount,
                CurrentCount = slidingWindow.CurrentCount,
                PreviousWeight = Math.Round(previousWindowWeight, 2),
                WeightedCount = Math.Round(weightedRequestCount, 2),
            }.ToString());

            return weightedRequestCount > _options.MaxRequestsPerWindow;
        }

        // has to be locked for thread safety / in distributed scenario
        public async Task LogRequest(string key)
        {
            var slidingWindow = await _repository.GetSlidingWindow(key);
            var currentTimestamp = DateTime.UtcNow;
            var elapsedSinceWindowStart = currentTimestamp - slidingWindow.StartTimestamp;

            // if current window duration is exceeded (new window started)
            if (elapsedSinceWindowStart > _options.WindowDuration)
            {
                // if current window duration is exceeded by more than 2x it means that previous window data is too old and won't affect calculation
                // thus current window can be treated as a fresh one
                if (elapsedSinceWindowStart > _options.WindowDuration * 2)
                {
                    slidingWindow = slidingWindow with
                    {
                        StartTimestamp = currentTimestamp,
                        PreviousCount = 0,
                        CurrentCount = 0
                    };
                }
                // otherwise we start a new current window and old current becomes previous
                else
                {
                    slidingWindow = slidingWindow with
                    {
                        StartTimestamp = slidingWindow.StartTimestamp + _options.WindowDuration,
                        PreviousCount = slidingWindow.CurrentCount,
                        CurrentCount = 0
                    };
                }
            }

            slidingWindow = slidingWindow with
            {
                CurrentCount = slidingWindow.CurrentCount + 1
            };

            await _repository.SaveSlidingWindow(key, slidingWindow);
        }
    }
}
