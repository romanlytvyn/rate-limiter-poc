﻿namespace RateLimiter
{
    public interface IRateLimiter
    {
        public Task<bool> IsRateLimited(string key);

        public Task LogRequest(string key);
    }
}
