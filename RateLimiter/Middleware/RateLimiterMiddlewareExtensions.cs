﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace RateLimiter
{
    public static class RateLimiterMiddlewareExtensions
    {
        private const string DefaultRateLimiterConfigSection = "RateLimiter";

        public static void AddInMemoryRateLimiter(
            this IServiceCollection services,
            IConfiguration config,
            string rateLimiterOptionsSection = DefaultRateLimiterConfigSection)
        {
            var rateLimiterOptions = config.GetSection(rateLimiterOptionsSection).Get<RateLimiterOptions>();

            services.Configure<RateLimiterOptions>(config.GetSection(rateLimiterOptionsSection));

            if (rateLimiterOptions.RateLimiterType == RateLimiterType.SlidingLog)
            {
                services.AddSingleton<IRequestLogRepository, InMemoryRequestLogRepository>();
                services.AddScoped<IRateLimiter, SlidingLogRequestLimiter>();
            }

            if (rateLimiterOptions.RateLimiterType == RateLimiterType.SlidingWindow)
            {
                services.AddSingleton<ISlidingWindowRepository, InMemorySlidingWindowRepository>();
                services.AddScoped<IRateLimiter, SlidingWindowRequestLimiter>();
            }

            if (rateLimiterOptions.KeyExtractorType == KeyExtractorType.Path)
            {
                services.AddScoped<IRequestKeyExtractor, PathRequestKeyExtractor>();
            }
        }

        public static IApplicationBuilder UseRateLimiter(
            this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<RateLimiterMiddleware>();
        }
    }
}
