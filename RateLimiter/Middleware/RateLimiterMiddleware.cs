﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;

namespace RateLimiter
{
    public class RateLimiterMiddleware
    {
        private readonly RequestDelegate _next;

        public RateLimiterMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context,
            IOptions<RateLimiterOptions> rateLimiterOptions,
            IRateLimiter rateLimiter,
            IRequestKeyExtractor requestKeyExtractor)
        {
            if (!rateLimiterOptions.Value.IsEnabled)
            {
                await _next(context);
                return;
            }

            var key = requestKeyExtractor.GetKey(context.Request);

            await rateLimiter.LogRequest(key);

            if (await rateLimiter.IsRateLimited(key))
            {
                context.Response.StatusCode = StatusCodes.Status429TooManyRequests;
                return;
            }

            await _next(context);
        }
    }
}
