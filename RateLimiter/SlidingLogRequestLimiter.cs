﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace RateLimiter
{
    internal class SlidingLogRequestLimiter : IRateLimiter
    {
        private readonly RateLimiterOptions _options;
        private readonly IRequestLogRepository _repository;
        private readonly ILogger<SlidingLogRequestLimiter> _logger;

        public SlidingLogRequestLimiter(IOptions<RateLimiterOptions> options,
            IRequestLogRepository repository,
            ILogger<SlidingLogRequestLimiter> logger)
        {
            _options = options.Value;
            _repository = repository;
            _logger = logger;
        }

        public async Task<bool> IsRateLimited(string key)
        {
            var requestsCountPerWindow = await _repository.CountByKeyAndOffset(key, _options.WindowDuration);

            _logger.LogDebug(new
            {
                Timestamp = DateTime.UtcNow,
                WindowDuration = _options.WindowDuration,
                RequestsPerWindow = requestsCountPerWindow,
            }.ToString());

            return requestsCountPerWindow > _options.MaxRequestsPerWindow;
        }

        public async Task LogRequest(string key)
        {
            await _repository.AddRequestLog(key);
            await _repository.RemoveRequestLogs(_options.WindowDuration);
        }
    }
}
