﻿namespace RateLimiter
{
    internal interface IRequestLogRepository
    {
        public Task<int> CountByKeyAndOffset(string key, TimeSpan offset);

        public Task AddRequestLog(string key);

        public Task RemoveRequestLogs(TimeSpan offset);
    }
}
