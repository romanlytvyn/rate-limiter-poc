﻿namespace RateLimiter
{
    internal class InMemoryRequestLogRepository : IRequestLogRepository
    {
        private readonly List<RequestLogItem> _requestLog = new();

        public async Task<int> CountByKeyAndOffset(string key, TimeSpan offset)
        {
            var count = _requestLog.Count(l => l.Key == key && l.Timestamp > DateTime.UtcNow - offset);
            return await Task.FromResult(count);
        }

        public async Task AddRequestLog(string key)
        {
            _requestLog.Add(new RequestLogItem(DateTime.UtcNow, key));
            await Task.CompletedTask;
        }

        public async Task RemoveRequestLogs(TimeSpan offset)
        {
            _requestLog.RemoveAll(l => l.Timestamp < DateTime.UtcNow - offset);
            await Task.CompletedTask;
        }
    }
}
