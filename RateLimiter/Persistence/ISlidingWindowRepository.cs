﻿namespace RateLimiter
{
    internal interface ISlidingWindowRepository
    {
        public Task<SlidingWindow> GetSlidingWindow(string key);

        public Task SaveSlidingWindow(string key, SlidingWindow slidingWindow);
    }
}
