﻿using System.Collections.Concurrent;

namespace RateLimiter
{
    internal class InMemorySlidingWindowRepository : ISlidingWindowRepository
    {
        private readonly ConcurrentDictionary<string, SlidingWindow> _slidingWindows = new();

        public async Task<SlidingWindow> GetSlidingWindow(string key)
        {
            var window = _slidingWindows.GetOrAdd(key, new SlidingWindow(DateTime.UtcNow, 0, 0));
            return await Task.FromResult(window);
        }

        public async Task SaveSlidingWindow(string key, SlidingWindow slidingWindow)
        {
            _slidingWindows.AddOrUpdate(key, slidingWindow, (_,_) => slidingWindow);
            await Task.CompletedTask;
        }
    }
}
