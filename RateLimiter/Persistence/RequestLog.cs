﻿namespace RateLimiter
{
    internal record RequestLogItem(DateTime Timestamp, string Key);
}
