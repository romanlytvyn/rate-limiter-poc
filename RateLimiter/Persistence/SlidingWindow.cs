﻿namespace RateLimiter
{
    internal record SlidingWindow(DateTime StartTimestamp, int PreviousCount, int CurrentCount);
}
