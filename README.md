## Description

A sample project showcasing different rate limiting strategies based on in-memory storage.

## Rate Limiting Strategies

Currently two strategies are implemented in `RateLimiter` library:

- Sliding Log
- Sliding Window

For more details on rate limiting algorithms [see here](https://www.quinbay.com/blog/understanding-rate-limiting-algorithms).

## Usage

To use `RateLimiter` library in the project, add the following to application startup.

``` CSharp
// register rate limiter services
builder.Services.AddInMemoryRateLimiter(builder.Configuration);

// use rate limiter middleware
// include as early in the pipeline as possible
app.UseRateLimiter();
```

## Configuration

Rate limiting is configured by `RateLimiter` section in `appsettings.json`.

``` JSON
"RateLimiter": {
    "RateLimiterType": "SlidingWindow", // SlidingWindow, SlidingLog
    "KeyExtractorType": "Path", // rate limit based on request path key
    "MaxRequestsPerWindow": 2,
    "WindowDurationSeconds": 10
}
```
